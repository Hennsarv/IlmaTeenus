﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace IlmaTeenus
{
    public partial class Ilmateenus : ServiceBase
    {
        public Ilmateenus()
        {
            InitializeComponent();
            ilmaEvent = new EventLog();
            if (EventLog.SourceExists("Ilmalogi"))
            {
                EventLog.CreateEventSource("Ilmateenus", "Ilmalogi");
            }
            ilmaEvent.Source = "Ilmateenus";
            ilmaEvent.Log = "Ilmalogi";
        }

        protected override void OnStart(string[] args)
        {
            ilmaEvent.WriteEntry("Ilmateenus käivitus");
        }

        protected override void OnStop()
        {
            ilmaEvent.WriteEntry("Ilmateenus peatus");
        }
    }
}
